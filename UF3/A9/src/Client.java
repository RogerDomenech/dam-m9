import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client extends Thread{
	//propietats
	Socket socket;//socket client
	BufferedReader fentrada; // entrada de dades dels client
	PrintWriter fsortida; // sortdia de dades dle client
	long id; //id del client
	String name; //nom del client
	//constructor

	public Client(Socket socket, String name) {
		// crear un fil
		Thread client = new Thread();
		this.id=client.getId();
		this.name= name;
		this.socket=socket;
		client.start();
	}

	public void run() {

		try {
			this.fentrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		};
		try {
			this.fsortida = new PrintWriter(this.socket.getOutputStream(), true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
	}
	//gets&sets

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	public BufferedReader getFentrada() {
		return fentrada;
	}

	public void setFentrada(BufferedReader fentrada) {
		this.fentrada = fentrada;
	}

	public PrintWriter getFsortida() {
		return fsortida;
	}

	public void setFsortida(PrintWriter fsortida) {
		this.fsortida = fsortida;
	}

	public long getId() {
		return id;
	}
	public void close() throws Exception {

		this.fentrada.close();
		fsortida.close();
		socket.close();

	}

	public String getClientName() {
		return name;
	}
	

}
