import java.io.IOException;
import java.io.Writer;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;

import org.apache.commons.net.smtp.AuthenticatingSMTPClient;
import org.apache.commons.net.smtp.SMTPReply;
import org.apache.commons.net.smtp.SimpleSMTPHeader;


public class ClientSMTP3 {

	public static void main (String[] args){


		// variables
		ArrayList<String>data = new ArrayList<String>();

		//lectura de parmentres
		if(args.length!=0) {
			for (int i = 0; i<args.length; i++) {
				//0 = to, 1 = from, 2 = cc, 3=bcc, 4=subject, 5 = missage
				data.add(args[i]);

			}

		}else {

			System.out.println("Introduccio manual: ");
			Scanner teclat = new Scanner(System.in);
			//introir to:
			System.out.println("TO: ");
			data.add(teclat.nextLine());
			//introduir from:
			System.out.println("FROM: ");
			data.add(teclat.nextLine());
			//introduir cc:
			System.out.println("CC: ");
			data.add(teclat.nextLine());
			//introduir bcc:
			System.out.println("BBC: ");
			data.add(teclat.nextLine());
			//introduir Subject:
			System.out.println("SUBJECT: ");
			data.add(teclat.nextLine());
			//introduir Missage:
			System.out.println("MISSAGE: ");
			data.add(teclat.nextLine());
		}


		// enviem correu
		try {
			send(data);
		} catch (UnrecoverableKeyException | InvalidKeyException | NoSuchAlgorithmException | KeyStoreException
				| InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void send(ArrayList<String> parameters) throws NoSuchAlgorithmException, UnrecoverableKeyException, KeyStoreException, InvalidKeyException, InvalidKeySpecException {
		//Es crea el client SMTP segur
		AuthenticatingSMTPClient client = new AuthenticatingSMTPClient();

		//Dades d'usuari i del servidor
		String server = "smtp.gmail.com";
		String username = "roger.domenech@insbaixcamp.cat";
		String contrasenya = "insbc9293";
		int port = 587;
		//Dades d'eviament
		//to
		String desti1 = parameters.get(0);
		//from
		String from = parameters.get(1);
		//CC
		String cc=parameters.get(2);
		//BCC
		String bcc=parameters.get(3);
		//subject
		String asumpte = parameters.get(4);
		//missatge
		String missatge = parameters.get(5);

		try {

			int resposta;
			//Creaci� de la clau per establir el canal segur
			KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
			kmf.init(null, null);
			KeyManager km = kmf.getKeyManagers() [0];

			//Ens connectem al servidor SMTP
			client.connect(server, port);
			System.out.println("1 - "+client.getReplyString());

			//S'estableix la clau per a la comunicaci� segura
			client.setKeyManager(km);

			resposta = client.getReplyCode();
			if (!SMTPReply.isPositiveCompletion(resposta)) {

				client.disconnect();
				System.err.println("CONNEXI� REBUTJADA");
				System.exit(1);

			}

			//S'envia l'ordre EHLO
			client.ehlo(server);//Cal fer-ho
			System.out.println("2 - "+client.getReplyString());

			//S'executa l'ordre STARTTLS i es comprova si �s true
			if (client.execTLS()) {

				System.out.println("3 -"+client.getReplyString());

				//Es fa l'autenticaci� amb el servidor
				if (client.auth(AuthenticatingSMTPClient.AUTH_METHOD.PLAIN, username, contrasenya)) {

					System.out.println("4 -"+client.getReplyString());

					//Es crea la cap�alera
					SimpleSMTPHeader capcalera = new SimpleSMTPHeader(from, desti1+",CC:"+cc+",BCC:"+bcc, asumpte);

					//El nom d'usuari i el email d'origen coincideixen
					client.setSender(from);
					client.addRecipient(desti1);
					System.out.println("5 -"+client.getReplyString());

					//S'envia DATA
					Writer writer = client.sendMessageData();

					if (writer == null) {
						System.out.println("ERRADA al enviar DATA");
						System.exit(1);
					}

					writer.write(capcalera.toString());//Cap�alera
					writer.write(missatge);//El missatge
					writer.close();
					System.out.println("6 -"+client.getReplyString());

					boolean exit = client.completePendingCommand();
					System.out.println("7 -"+client.getReplyString());

					if (!exit) {
						System.out.println("ERRADA al finalitzar la TRANSACCI�");
						System.exit(1);
					}

				} else {

					System.out.println("USUARI NO AUTENTICAT");

				}

			} else {

				System.out.println("ERRADA AL EXECUTAR STRATTLS");

			}

		} catch (IOException e) {

			System.out.println("No pot connectar amb el servidor");
			e.printStackTrace();
			System.exit(1);

		}

		try {
			client.disconnect();
		} catch (IOException f) {f.printStackTrace(); }

		System.out.println("Final de l'enviament");
		System.exit(0);

	}

}
