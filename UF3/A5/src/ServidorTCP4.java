import java.net.*;
import java.util.ArrayList;


import java.io.*;


public class ServidorTCP4 {

	
	public static void main (String[] args) throws Exception {
	
		try {
			//maxim clients
			int maxClient = Integer.valueOf(args[0]);
			// clients conectats
			int connected=0;
			// arraylist de clients
			ArrayList<Socket>Clients = new ArrayList<Socket>();
			//bolean control 
			boolean ctrl= (connected<maxClient)?true:false;

			int numPort = 60000;

			ServerSocket servidor = new ServerSocket(numPort);
			String cadena = "";
			// creacio dels clients
			for (int i = 0; i < maxClient; i++) {
				System.out.println("Esperant connexi� "+(i+1)+"... ");
				Socket client = servidor.accept();
				System.out.println("Client "+(i+1)+" connectat... ");
				connected++;
				Clients.add(client);
			}
			System.out.println("Clients conectats "+connected+"/"+maxClient+"\nEstat servidor:"+((ctrl)?"ok":"ERROR"));
			//ArrayList de les entrades i sortides de cada client
			ArrayList<PrintWriter> fsortides = new ArrayList<PrintWriter>();
			ArrayList<BufferedReader> fentrades = new ArrayList<BufferedReader>();
			for (int i = 0; i < connected; i++) {
				PrintWriter fsortida = new PrintWriter(Clients.get(i).getOutputStream(), true);
				BufferedReader fentrada = new BufferedReader(new InputStreamReader(Clients.get(i).getInputStream()));
				fsortides.add(fsortida);
				fentrades.add(fentrada);
			}
			//Inici de la conexio de cada client per ordre numeric d'entrada
			int count=0;
			while (ctrl&&connected!=0) {
				System.out.println("Esperant Client "+(count+1)+"...");
				//FLUX DE SORTIDA AL CLIENT
				try {
					PrintWriter fsortida = new PrintWriter(Clients.get(count).getOutputStream(), true);
					
					fsortida.println("Client "+(count+1)+" preparat" );

					//FLUX D'ENTRADA DEL CLIENT
					BufferedReader fentrada = new BufferedReader(new InputStreamReader(Clients.get(count).getInputStream()));

					while ((cadena = fentrada.readLine()) != null) {
						// Comprovacio de cas de tancament del servidor de part de client
						if (cadena.equals("*")||cadena.equalsIgnoreCase("server.close")) {
							cadena="EXIT";
							fsortida.println(cadena);
							ctrl=false;
						}else {
						// En cas contrari funcionament normal 
						fsortida.println(cadena);
						System.out.println("Rebent del Client "+(count+1)+": "+cadena);
						}
						System.out.println("Esperant Client "+(count+1)+"...");
					}

					//TANCAR STREAMS I SOCKETS
					System.out.println("Tancant connexi� del client "+(count+1)+" ... ");
					fentrada.close();
					fsortida.close();
					Clients.get(count).close();
					
					// control de la Excepcio de la sortida d'un client
				}catch (SocketException es) {
					// reduim el nombre de conectats
					connected--;
					// generem el proxim mumero de client a connectar-se
					count=(count<=maxClient)?count+1:0;
					// missatge de Estat del servidor
					System.out.println("Status:\nExeception: "+es.getMessage()+"\nConnected:"+connected);
					if(connected!=0) {
						System.out.println("Next Client: Client"+(count+1));
					}else{
						System.out.println("Have a nice day! :)");
					}
				}catch (Exception e) {
					//Controlem un error general 
					// mosterm la informacio del error
					e.printStackTrace();
					// tanquem servidor
					servidor.close();
					// tanquem programa
					System.exit(0);
				}

			}
			//en aquest punt no hi ha clients per tant tanquem servidor
			
			servidor.close();

		}catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Requerix paramatre indicant nombre de clients maxim\njava ServidorTCP4 [numero Maxim Clients]");
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}