import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;


import java.io.*;

public class ServidorTCP3{
	static class Server implements Callable<ArrayList<Client>>{
		// arraylist de clients
		private ArrayList<Client>conectats = new ArrayList<Client>();
		/*public Server(Client client) {
			this.conectats.add(client);
		}*/
		public Server(ArrayList<Client>clients) {
		this.conectats=clients;
		}
		private static void read(Client client) throws Exception {
			PrintWriter fsortida = new PrintWriter(client.getSocket().getOutputStream(), true);

			fsortida.println(client.getClientName()+" preparat" );

			//FLUX D'ENTRADA DEL CLIENT
			BufferedReader fentrada = new BufferedReader(new InputStreamReader(client.getSocket().getInputStream()));

			String cadena;
			while ((cadena = fentrada.readLine()) != null) {
				// Comprovacio de cas de tancament del servidor de part de client
				if (cadena.equals("*")||cadena.equalsIgnoreCase("server.close")) {
					cadena="EXIT";
					fsortida.println(cadena);
					cadena=null;
				}else {
					// En cas contrari funcionament normal 
					fsortida.println(cadena);
					System.out.println("Rebent del "+client.getClientName()+": "+cadena);
				}
			}

			//TANCAR STREAMS I SOCKETS
			System.out.println("Tancant connexi� del "+client.getClientName()+" ... ");
			client.close();
		}

		@Override
		public ArrayList<Client> call() {
			boolean ctrl= true;
			boolean isConected = true;
			
			//Inici de la conexio de cada client per ordre numeric d'entrada
			int count=0;
			while (ctrl) {
				try {
					while (isConected) {
						Client client= conectats.get(count);
						//read(client);
						PrintWriter fsortida = new PrintWriter(client.getSocket().getOutputStream(), true);

						//fsortida.println(client.getClientName()+" preparat" );

						//FLUX D'ENTRADA DEL CLIENT
						BufferedReader fentrada = new BufferedReader(new InputStreamReader(client.getSocket().getInputStream()));

						String cadena = (fentrada.ready())?fentrada.readLine():"";
						//while ((cadena = fentrada.readLine()) != null && ) {
							// Comprovacio de cas de tancament del servidor de part de client
							if (cadena.equals("*")||cadena.equalsIgnoreCase("server.close")) {
								cadena="EXIT";
								fsortida.println(cadena);
								isConected = false;
							}else if(cadena != ""){
								// En cas contrari funcionament normal 
								//fsortida.println(cadena);
								for (int i = 0; i < conectats.size(); i++) {
									if(conectats.get(i).getClientName()!= client.getClientName()) {
										//System.out.println(i);
										//conectats.get(i).getFsortida().println(client.getClientName()+" send => "+cadena);
										PrintWriter fsortidaClient = new PrintWriter(conectats.get(i).getSocket().getOutputStream(), true);
										fsortidaClient.println(client.getClientName()+" send => "+cadena);
									}
								}
								
								System.out.println("Rebent del "+client.getClientName()+": "+cadena);
							}else {
								//envia null
								//fsortida.println("");
							}
						//}
						if(!isConected) {
						//TANCAR STREAMS I SOCKETS
						System.out.println("Tancant connexi� del "+client.getClientName()+" ... ");
						client.close();
						}
						count=(count>=conectats.size())?0:count++;
					}
					
					// control de la Excepcio de la sortida d'un client
				}catch (NullPointerException es) {

					// missatge de Estat del servidor
					System.out.println(conectats.get(count).getClientName()+" desconectat");
					es.printStackTrace();
					conectats.remove(count);
					ctrl=false;

				}catch (Exception e) {
					//Controlem un error general 
					// mosterm la informacio del error
					e.printStackTrace();
					ctrl=false;
				}

			}
			return conectats;

		}

	}


	public static void main (String[] args) throws Exception {
		try {
			//maxim clients
			int maxClient = Integer.valueOf(args[0]);
			ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(maxClient);
			// clients conectats
			int connected=0;
			// arraylist de clients
			ArrayList<Client>listclients = new ArrayList<Client>();
			List<Server> llistaServer = new ArrayList<Server>();
			//bolean control 
			boolean ctrl= (connected<maxClient)?true:false;

			int numPort = 60000;

			ServerSocket servidor = new ServerSocket(numPort);

			// creacio dels clients
			// cada client tindra un fil que contrindra el socket
			for (int i = 0; i < maxClient; i++) {
				System.out.println("Esperant connexi� "+(i+1)+"... ");
				Client client= new Client(servidor.accept(),"Client "+i);
				//Server server = new Server(client);
				listclients.add(client);
				System.out.println(client.name+" connectat... ");
				connected++;
				//llistaServer.add(server);
			}
			Server server = new Server(listclients);
			llistaServer.add(server);
			System.out.println("Clients conectats "+connected+"/"+maxClient+"\nEstat servidor:"+((ctrl)?"ok":"ERROR"));
			//ArrayList de les entrades i sortides de cada client

			List<Future<ArrayList<Client>>> llistaClients;
			llistaClients= executor.invokeAll(llistaServer);
			executor.shutdown();
			System.out.println("Have a nice day! :)");

		}catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Requerix paramatre indicant nombre de clients maxim\njava ServidorTCP3 [numero Maxim Clients]");
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}

