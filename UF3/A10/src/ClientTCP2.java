import java.net.*;
import java.io.*;

public class ClientTCP2 {
	
	public static void main (String[] args) {
		
		String host = "localhost";
		int port = 60000;//Port remot
		Socket client=null;
		try {
			client = new Socket(host, port);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//FLUX DE SORTIDA AL SERVIDOR
		PrintWriter fsortida;
	
		
		//FLUX D'ENTRADA AL SERVIDOR
		BufferedReader fentrada;
		try {
			fsortida = new PrintWriter(client.getOutputStream(), true);
			fentrada = new BufferedReader(new InputStreamReader(client.getInputStream()));
			//FLUX PER A ENTRADA ESTĀNDARD
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("Esperant Servidor...");
			//esperem confirmacio del servidor
			//System.out.println(fentrada.readLine());
			String cadena, eco = "";
			System.out.println("Introdueix la cadena: ");
			//Lectura teclat
			cadena = in.readLine();
			if (!cadena.equalsIgnoreCase("exit")) {
				
				//Enviament cadena al servidor
				fsortida.println(cadena);
				//Rebuda cadena del servidor
				if(fentrada.ready()) {
				eco = fentrada.readLine();
				System.out.println("  =>ECO: "+eco);
				}
				//Lectura del teclat
				cadena = in.readLine();
				
				
			}else {
			
			fsortida.close();
			fentrada.close();
			System.out.println("Finalitzaciķ de l'enviament...");
			in.close();
			client.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
		
}
