import java.net.*;

public class Exemple1URL {
	
	public static void main (String[] args) {
		URL url;
		
		try {
			
			System.out.println("Constructor simple per a un URL: ");
			url = new URL("http://docs.oracle.com/");
			Visualitzar (url);
			
			System.out.println("Altra constructor simple per a un URL: ");
			url = new URL("http://localhost/testhttp/");
			Visualitzar (url);
			
			System.out.println("Constructor per a protocol + URL + directori: ");
			url = new URL("http", "doc.oracle.com", "/javase/7");
			Visualitzar (url);
			
			System.out.println("Constructor per a protocol + URL + port + directori: ");
			url = new URL("http", "doc.oracle.com", 80, "/javase/7");
			Visualitzar (url);
			
			System.out.println("Constructor per a un objecte URL i un directori: ");
			URL urlBase = new URL("http://docs.oracle.com/");
			url = new URL(urlBase, "/javase/7/docs/api/java/net/URL.html");
			Visualitzar (url);
			
		} catch (MalformedURLException e) { System.out.println(e); }
		
	}
	
	private static void Visualitzar(URL url) {
		//url completa
		System.out.println("\tURL complerta: "+url.toString());
		//protocol de ul
		System.out.println("\tgetProtocol: "+url.getProtocol());
		//host
		System.out.println("\tgetHost: "+url.getHost());
		//port
		System.out.println("\tgetPort: "+url.getPort());
		//retorna el file de la url
		System.out.println("\tgetFile: "+url.getFile());
		//retorna la informacio d'usuari [marcat amb @]
		System.out.println("\tgetUserInfo: "+url.getUserInfo());
		//retorna el Path de la url 
		System.out.println("\tgetPath: "+url.getPath());
		//retorna el host sense el protocol
		System.out.println("\tgetAuthority: "+url.getAuthority());
		//retorna la query en get ?= 
		System.out.println("\tgetQuery: "+url.getQuery());
		System.out.println("=====================================================");
	}

}
