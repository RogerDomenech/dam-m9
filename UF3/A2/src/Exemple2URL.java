import java.net.*;
import java.io.*;

public class Exemple2URL {
	
	public static void main (String[] args) {
		URL url=null;
		
		try {
			//http://127.0.0.1:8080/testhttp/
			//url = new URL("http://www.catastro.meh.es");
			url = new URL("http://localhost/testhttp/"); 
			
		} catch (MalformedURLException e) {e.printStackTrace(); }
		
		BufferedReader in;
		
		try {
			
			// obre el contingut de la url 
			InputStream inputStream = url.openStream();
			// crea un Buffer de lecura del seu contingut
			in = new BufferedReader(new InputStreamReader(inputStream));
			
			String inputLine;
			// per cada linea
			while ((inputLine = in.readLine()) != null)
				// impimeix la lineea actual
				System.out.println(inputLine);
			// tanca el buffer
			
			in.close();
			
		} catch (IOException e) {e.printStackTrace(); }

	}
}
