import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.URL;

public class A2 {

	public static void main (String[] args) {
		// variables 
		URL url=null;
		URL baseUrl =null;
		String port = args[1];
		//Controlem possibles errors
		try {
			//montem ulr amb el primer paramentre passat
			baseUrl = new URL(args[0]); 
			// montem un string per montar la url a comprovar posteriorment
			// amb la seguent estructura [protocol][host][port][path]
			String urlString= new String(baseUrl.getProtocol()+"://"+baseUrl.getHost()+":"+port+baseUrl.getPath());
			// montem la Url 
			url= new URL(urlString);
		} catch (MalformedURLException e) {
			// en cas de mal formacio mostrem error i tanquem program 
			System.out.println(e.getMessage());
			System.exit(0);
		}
		
		/// analitzem el contingut de la url passada 
		// buffer que contindra el html 
		BufferedReader in;
		//controlem errors
		try {
			//Creem una entrada de dades amb la url 
			InputStream inputStream = url.openStream();
			// iniciem el buffer amb la entrada de dades feta anteriorment
			in = new BufferedReader(new InputStreamReader(inputStream));
			//variable de la linea
			String inputLine;
			//menteres que la linea sigui diferent de null llegiex 
			while ((inputLine = in.readLine()) != null)
				//printa el contigut de la linea
				System.out.println(inputLine);
			// tanca el buffer
			in.close();
			// controlem el error de conexio 
		} catch (ConnectException e) {
			System.out.println(e.getMessage());
			// controlem el error general 
		} catch (IOException e) {
			e.printStackTrace(); 
		}

	}
}

// direcions exemple 
// http://www.catastro.meh.es
//http://testhttp.test
//http:localhost:8000/testhttp/
//http:localhost/testhttp/
