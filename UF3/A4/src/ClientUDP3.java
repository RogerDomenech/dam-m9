import java.net.*;
import java.io.*;
import java.io.ObjectInputStream.GetField;

public class ClientUDP3 {
	
	public static void main (String[] args) throws Exception {
		
		//FLUX PER A ENTRADA ESTĀNDARD
				BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
				//Socket client
				DatagramSocket clientSocket = new DatagramSocket();
				byte[] enviats = new byte[1024];
				byte[] rebuts = new byte[1024];
				//temps de Timeout
				int time = 5000;
				//boolan de control del while
				boolean ctrl=true;
				//DADES DEL SERVIDOR al qual s'envia el missatge
				InetAddress IPServidor = InetAddress.getLocalHost();
				int port = 9800;
				
				while(ctrl){
			    clientSocket.setSoTimeout(time);
			    
			    try {
				//INTRODUIR DADES PEL TECLAT
				System.out.print("Introdueix missatge: ");
				String cadena = in.readLine();
				enviats = cadena.getBytes();
				
				//ENVIANT DATAGRAMA AL SERVIDOR
				System.out.println("Enviant "+enviats.length+"bytes al servidor.");
				DatagramPacket enviament = new DatagramPacket(enviats, enviats.length, IPServidor, port);
				clientSocket.send(enviament);
				
				
				
				//REBENT DATAGRAMA DEL SERVIDOR
				DatagramPacket rebut = new DatagramPacket(rebuts, rebuts.length);
				System.out.println("Esperant datagrama...");
				
				
					clientSocket.receive(rebut);
					String majuscula = new String(rebut.getData());

					//ACONSEGUINT INFORMACIĶ DEL DATAGRAMA
					InetAddress IPOrigen = rebut.getAddress();
					int portOrigen = rebut.getPort();
					System.out.println("\tProcedent de: "+IPOrigen+":"+portOrigen);
					System.out.println("\tDades: "+majuscula.trim());
					//comanda que tanca el client
					if(majuscula.trim().equalsIgnoreCase("Client.close")) {
						System.out.println("Have a nice day! :)");
						ctrl=false;
					}
				} catch (SocketTimeoutException e) {
					System.out.println("No se reven dades");
					// trenquem el while
					ctrl=false;
				}
				
				}
				
				//Tanca el socket
				clientSocket.close();
		
		}

		
	}
		

