import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

public class Notaria {

	/**
	 * Funcio de lectura dels fitxers
	 * @param archive Nom amb extencio del fitxer 
	 * @return filebytes bytes amb el contingut del fitxer 
	 * */ 
	public static byte[] fileReader(String archive){
		String absolutePath = new File(archive).getAbsolutePath();
		byte[] fileByte =null;
		try {
			fileByte = Files.readAllBytes(Paths.get(absolutePath));
		} catch (Exception e) {
			System.out.println("Error\n"+e);
		}
		return fileByte;	
	}

	/**
	 * Validacio de la Signatura
	 * @param data byte[] del arixiu a comprovar
	 * @param signature byte[] del arixu de la signatura
	 * @param pub PublicKey clau publica
	 * @return boolea
	 */
	public static boolean validateSignature(byte[] data, byte[] signature, PublicKey pub) {
		boolean isValid = false;
		try {
			Signature signer = Signature.getInstance("SHA1withRSA");
			signer.initVerify(pub);
			signer.update(data);
			isValid = signer.verify(signature);
		}
		catch (Exception ex) {
			System.err.println("Error validant les dades: " + ex);
		}
		return isValid;
	}


	/**
	 * Genera una Clau publica aprtir el arxiu
	 * @param filename String del directori del arxiu
	 * @return Publickey del arxiu
	 */
	public static PublicKey generatePublickey(String filename) {
		// llegim el arixiu de la keyPublic
		byte[] fileBytePublic = fileReader(filename);
		// passem de bye a keypublic, utilitzem KeyFactory
		KeyFactory factory=null;
		try {
			factory = KeyFactory.getInstance("RSA");
		} catch (Exception e) {
			System.out.println("Error KeyFactory: "+e);
		}
		// utilitzem el methode generatePublic amb el format X509EncodedKeySpec
		PublicKey keyPublic=null;
		try {
			keyPublic= factory.generatePublic(new X509EncodedKeySpec(fileBytePublic));
		} catch (Exception e) {
			System.out.println("Error KeyPublic: "+e);
		}

		return keyPublic;
	}
	/**
	 * Method de desencrytacio dades
	 * @param data byte[] text en byte
	 * @param pub Privatekey clau privada
	 * @return retorna byte[] contingut 
	 */

	public static byte[] dencryptData(byte[] data, PrivateKey pub) {
		byte[] encryptedData = null;
		try {
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding","SunJCE");
			cipher.init(Cipher.DECRYPT_MODE, pub);
			encryptedData = cipher.doFinal(data);
		}
		catch (Exception ex) {
			System.err.println("Error xifrant: " + ex);
		}
		return encryptedData;
	}
	/**
	 * Generador de Privateskey
	 * @param namefile String ruta del arixiu 
	 * @return PrivateKey generada apartir del arxiu 
	 */
	public static PrivateKey generatePrivatekey (String namefile) {
		// desencriptem amb la clau privada
		byte[] clauPrivadaByte = fileReader(namefile);
		// passem de bye a private, utilitzem KeyFactory
		KeyFactory factory=null;
		try {
			factory = KeyFactory.getInstance("RSA");
		} catch (Exception e) {
			System.out.println("Error KeyFactory: "+e);
		}
		// utilitzem el methode generatePublic amb el format PKCS8EncodedKeySpec
		PrivateKey keyPrivate=null;
		try {
			keyPrivate = factory.generatePrivate(new PKCS8EncodedKeySpec(clauPrivadaByte));

		} catch (Exception e) {
			System.out.println("Error PrivateKey: "+e);
		}
		return keyPrivate;
	}

	public static void main(String[] args) {
		System.out.print("Comprovant signatura de l�arxiu missatge...");
		// check si existeis missatge
		File missatge = new File ("missatge");
		if(missatge.exists()&& missatge.isFile()) {
			byte[] missatgeData = fileReader("missatge");
			// check si hi ha el arxiu firma_missatge
			File firmaMissatge = new File("firma_missatge");
			if(firmaMissatge.exists() && firmaMissatge.isFile()) {
				// generem la clauprivada
				//PrivateKey privatekeyGen = generatePrivatekey("clauPrivada");
				// Descodifi missatge
				//byte[] missatgeDataDescript = dencryptData(missatgeData, privatekeyGen);
				// llegir arxiu firma_missatge
				byte[] fileFirma_missatge = fileReader("firma_missatge");
				// comprovem la existencia del arixu clauPublica
				File clauPublica = new File("clauPublica");
				if(clauPublica.exists()&&clauPublica.isFile()) {
					// llegim arixu clauPublica
					PublicKey keypublic= generatePublickey("clauPublica");
					// comprovar si els arixus generats son correctes
					//boolean checkFirma = validateSignature(missatgeDataDescript, fileFirma_missatge, keypublic);
					boolean checkFirma = validateSignature(missatgeData, fileFirma_missatge, keypublic);
					if(checkFirma) {
						System.out.println("OK");
					}else {
						System.out.println("FAIL");
					}
				}

			}

		}
	}

}
