import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Scanner;

import javax.crypto.Cipher;

public class Signatura {
	private static int keySize = 512;

	/**
	 * Genera un parell de claus segons logintiud introduida.
	 * @param longuitudClau
	 * @return keys conte la clau publica y la clau privada
	 */
	public static KeyPair randomGenerate() {
		KeyPair keys = null;
		try {
			KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
			keyGen.initialize(keySize);
			keys = keyGen.genKeyPair();
		}
		catch (Exception ex) {
			System.err.println("Generador no disponible.");
		}
		return keys;
	}


	/**
	 * Funcio de escriptura dels fitxers
	 * @param filebyte bytes amb el contingut del fitxer 
	 * @param filename String amb el nom d'arxiu sortida
	 */
	public static void fileWrite(byte[] filebyte, String filename) {

		try {

			File file = new File(filename);
			//check si existeix arxiu
			if(!file.exists()) {
				file.createNewFile();
			}
			//omplim el arixu amb els byte
			FileOutputStream fileStream = new FileOutputStream(file);
			fileStream.write(filebyte);
			fileStream.flush();
			fileStream.close();
			//avis usuari
			//System.out.println("\nFitxer: "+filename+" creat correctament");
		} catch (Exception e) {
			System.out.println("\nError al crear el fitxer: "+filename+"\nError:"+e);
		}

	}

	/**
	 * Funcio de lectura dels fitxers
	 * @param archive Nom amb extencio del fitxer 
	 * @return filebytes bytes amb el contingut del fitxer 
	 * */ 
	public static byte[] fileReader(String archive){
		String absolutePath = new File(archive).getAbsolutePath();
		byte[] fileByte =null;
		try {
			fileByte = Files.readAllBytes(Paths.get(absolutePath));
		} catch (Exception e) {
			System.out.println("Error\n"+e);
		}
		return fileByte;	
	}


	/**
	 * Method de signatura d'arxius
	 * @param data byte[] conte les dades en byte
	 * @param priv PrivateKey clau privada per codificar
	 * @return byte[] byte firmats amb la clau Privada
	 */

	public static byte[] signData(byte[] data, PrivateKey priv) {
		byte[] signature = null;
		try {
			Signature signer = Signature.getInstance("SHA1withRSA");
			signer.initSign(priv);
			signer.update(data);
			signature = signer.sign();
		}
		catch (Exception ex) {
			System.err.println("Error signant les dades: " + ex);
		}
		return signature;
	}

	/**
	 * Generador de Privateskey
	 * @param namefile String ruta del arixiu 
	 * @return PrivateKey generada apartir del arxiu 
	 */
	public static PrivateKey generatePrivatekey (String namefile) {
		// desencriptem amb la clau privada
		byte[] clauPrivadaByte = fileReader(namefile);
		// passem de bye a private, utilitzem KeyFactory
		KeyFactory factory=null;
		try {
			factory = KeyFactory.getInstance("RSA");
		} catch (Exception e) {
			System.out.println("Error KeyFactory: "+e);
		}
		// utilitzem el methode generatePublic amb el format PKCS8EncodedKeySpec
		PrivateKey keyPrivate=null;
		try {
			keyPrivate = factory.generatePrivate(new PKCS8EncodedKeySpec(clauPrivadaByte));

		} catch (Exception e) {
			System.out.println("Error PrivateKey: "+e);
		}
		return keyPrivate;
	}
	/**
	 * Genera una Clau publica aprtir el arxiu
	 * @param filename String del directori del arxiu
	 * @return Publickey del arxiu
	 */
	public static PublicKey generatePublickey(String filename) {
		// llegim el arixiu de la keyPublic
		byte[] fileBytePublic = fileReader(filename);
		// passem de bye a keypublic, utilitzem KeyFactory
		KeyFactory factory=null;
		try {
			factory = KeyFactory.getInstance("RSA");
		} catch (Exception e) {
			System.out.println("Error KeyFactory: "+e);
		}
		// utilitzem el methode generatePublic amb el format X509EncodedKeySpec
		PublicKey keyPublic=null;
		try {
			keyPublic= factory.generatePublic(new X509EncodedKeySpec(fileBytePublic));
		} catch (Exception e) {
			System.out.println("Error KeyPublic: "+e);
		}

		return keyPublic;
	}

	/**
	 * Method de encrytacio dades
	 * @param data byte[] text en byte
	 * @param pub Publickey clau publica
	 * @return retorna byte[] contingut 
	 */

	public static byte[] encryptData(byte[] data, PublicKey pub) {
		byte[] encryptedData = null;
		try {
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding","SunJCE");
			cipher.init(Cipher.ENCRYPT_MODE, pub);
			encryptedData = cipher.doFinal(data);
		}
		catch (Exception ex) {
			System.err.println("Error xifrant: " + ex);
		}
		return encryptedData;
	}



	public static void main(String[] args) {
		// boolean controlGeneracio
		boolean ctrlGeneracio = false;
		// boolean controlFirma
		boolean ctrlFirma = false;
		// generacio arixus claus publica i privada
		// generacio de keys random
		System.out.print("Generant claus publiques i provades (arxius clauPublica i clauPrivada)...");
		KeyPair keys = randomGenerate();
		//keyPublic
		PublicKey keyPublic = keys.getPublic();
		byte[] keyPublicData = keyPublic.getEncoded();
		//generacio del arxiu clauPublica
		fileWrite(keyPublicData, "clauPublica");
		//keyPrivate
		PrivateKey keyPrivate = keys.getPrivate();
		byte[] keyPrivateData = keyPrivate.getEncoded();
		//generacio arxiu clauPrivada
		fileWrite(keyPrivateData, "clauPrivada");
		// check exitis files
		File clauPublica = new File("clauPublica");
		File clauPrivada = new File("clauPrivada");
		if((clauPrivada.exists()&&clauPublica.exists())&&(clauPrivada.isFile()&&clauPublica.isFile())) {
			System.out.println("OK");
			ctrlGeneracio=true;
		}else {
			System.out.println("FAIL");
		}
		if(ctrlGeneracio) {
			// introduir missatge
			System.out.println("Introdueix el missatge a signar:");
			Scanner teclat = new Scanner(System.in);
			String missatge = teclat.nextLine();
			teclat.close();
			System.out.print("Signant el missatge...");
			byte[] missatgeData = missatge.getBytes();
			// llegim la clau privada
			PrivateKey keyPrivadaFile = generatePrivatekey("clauPrivada");
			if (keyPrivadaFile != null) {
				// codifiquem missatge 
				byte[] misstgeSignat = signData(missatgeData, keyPrivadaFile);
				System.out.println("OK");
				// generem arxiu firma_missatge
				System.out.print("Generant arxiu firma_missatge....");
				fileWrite(misstgeSignat, "firma_missatge");
				File fileFirma_missatge = new File("firma_missatge");
				// comprovem la creacio del arxiu 
				if( fileFirma_missatge.exists()&& fileFirma_missatge.isFile()) {
					System.out.println("OK");
					ctrlFirma = true;
				}else {
					System.out.println("FAIL");
				}
				if(ctrlFirma) {
					// llegim clauPublica
					//PublicKey keyPublicFile = generatePublickey("clauPublica");
					// encriptem el missatge 
					System.out.print("Generant arxiu missatge...");
					//byte[] missatgeEncript = encryptData(missatgeData, keyPublicFile);
					// generem arxiu misstage
					//fileWrite(missatgeEncript, "missatge");
					fileWrite(missatgeData, "missatge");
					File fileMissatge = new File("missatge");
					if (fileMissatge.exists()&& fileMissatge.isFile()) {
						System.out.println("OK");
					}else {
						System.out.println("FALSE\nError en la Generacio del arxiu missatge");
					}
				}else {
					System.out.println("Error en la Generacio del arxiu firma_missatge");
				}
			}else {
				System.out.println("Error en generar la clau Privada");
			}	
		}else {
			System.out.println("Error en la generació d'arxius");
		}




	}

}
