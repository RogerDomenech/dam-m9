package Encript;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Scanner;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;


public class A4 {
	//Colors Text
	public static String redColor = "\u001B[91m";
	public static String yellowColor = "\u001b[33m";
	public static String greenColor = "\u001b[32m";
	public static String cianColor = "\u001b[36m";
	public static String resetColor = "\u001b[0m";

	/**
	 * Funcio que genera un SecretKey
	 * @param  text text a codificar 
	 * @return SecretKey sKey objecte amb la key creada
	 */

	public static SecretKey passwordKeyGeneration(String text ) {
		SecretKey sKey= null;
		String codeHash = "MD5";
		int keySize = 128;

		try {
			byte[] data= text.getBytes("UTF-8");
			MessageDigest md = MessageDigest.getInstance(codeHash);
			byte[] hash = md.digest(data);
			byte[] key = Arrays.copyOf(hash, keySize/8);
			sKey = new SecretKeySpec(key,"AES");
		}catch(Exception ex) {
			System.err.println("Error generant la clau: "+ex);
		}
		return sKey;
	}
	/*
	 * Encriptacio 
	 * @param  sKey Secretkey genererat 
	 * @param  data entrada passada a binari
	 * @return text binari codificat
	 */
	public static byte[] encryptData(SecretKey sKey, byte[] data) {
		byte[] encryptedData = null;
		try {
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, sKey);
			encryptedData = cipher.doFinal(data);
		}
		catch (Exception ex) {
			System.err.println("Error xifrant les dades: " + ex);
		}
		return encryptedData;
	}
	/*
	 * DesEncriptacio 
	 * @param SecretKey sKey Secretkey genererat 
	 * @param byte data byte encriptat
	 * @return byte text binari codificat
	 */
	public static byte[] desEncryptData(SecretKey sKey, byte[] data) {
		byte[] encryptedData = null;
		try {
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, sKey);
			encryptedData = cipher.doFinal(data);
		}
		catch (Exception ex) {
			System.err.println("Error xifrant les dades: " + ex);
		}
		return encryptedData;
	}
	/**
	 * Interface d'usuari per la codificacio de text
	 */
	public static void codeText(String text,String key ) {

		byte[] data = text.getBytes();
		//encriptacio
		SecretKey sKey= passwordKeyGeneration(key);
		byte[] encryptData = encryptData(sKey, data);
		//descriptacio
		byte[] desEncryptData = desEncryptData(sKey, encryptData);
		String textDescrypt = new String(desEncryptData);
		try {

			int count =0;

			for (byte b : encryptData) {
				System.out.print(String.format("%8s",Integer.toBinaryString(b & 0xFF)).replace(' ','0')+" => ");
				System.out.println(encryptData[count]);

				count++;
			}

			System.out.println("\nText Descodificat es: "+yellowColor+textDescrypt+resetColor+"\nLength keys => "+encryptData.length+resetColor);
		} catch (Exception e) {
			System.out.println(redColor+"Error");
		}

	}

	/**
	 * Interfice usuari per codificar fitxers
	 */
	public static void codeFile(String archive, String key ) {	
		try {
			byte[] filebytes = ManageFile.fileReader(archive);
			//codificar
			SecretKey sKey= passwordKeyGeneration(key);
			byte[] encryptData = encryptData(sKey, filebytes);
			ManageFile.fileWrite(archive, encryptData, true);
			//descodificar
			byte[] desEncryptData = desEncryptData(sKey, encryptData);
			ManageFile.fileWrite(archive, desEncryptData, false);

		} catch (Exception e) {
			System.out.println("\nERROR: "+e);
		}

	}

	public static void main(String[] args) {
		Scanner teclat = new Scanner(System.in);

		boolean exit = true;
		boolean ctrl = true;
		while(exit) {
			String opcio ="";
			while (ctrl) {
				System.out.println("Escull opcio: \n0 => Codificar text\n1 => Codificar Fitxer\n2 => Sortir");
				opcio = teclat.next();

				ctrl=(opcio.equals("0") || opcio.equals("1") ||opcio.equals("2"))?false:true;
				
			}
			teclat.nextLine();
			if (opcio.equals("0")) {
				System.out.println("Introdueix text");
				String text = teclat.nextLine();
				//String text = "qwerty";

				System.out.println("Introdueix clau");
				String key = teclat.next();
				//String key = "q";
				//codificacio de text
				codeText(text,key );
			}
			if (opcio.equals("1")) {
				//iniciem clase ManageFile indicant el directori;
				ManageFile fileclass = new ManageFile("files");
				System.out.println("Introdueix el nom i extencio del Fitxer dintre del directori "+fileclass.getDir());
				String archive = teclat.next();
				System.out.println("Introdueix clau");
				String key = teclat.next();
				//codificacio de fitxter
				codeFile(archive,key);
			}
			System.out.println("\nVols sortir? Si | No");
			String sortir = teclat.next();
			exit = (sortir.equalsIgnoreCase("si"))?false:true;
			ctrl = exit;
		}
		teclat.close();
	}

}
