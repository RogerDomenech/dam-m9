package Encript;


import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

class ManageFile {
	//directori dels arixus
	public static String dir;
	
	public ManageFile(String dir) {
		ManageFile.dir = dir+"//";
	}
	public static String getDir() {
		return dir.substring(0,dir.length()-2);
	}
	public  String urlDir(String archive) {
		return new File(dir+archive).getAbsolutePath();
	}
	
	
	/**
	 * Funcio de lectura dels fitxers
	 * @param archive Nom amb extencio del fitxer 
	 * @return filebytes bytes amb el contingut del fitxer 
	 * */ 
	public static byte[] fileReader(String archive){
		String absolutePath = new File(dir+archive).getAbsolutePath();
		byte[] fileByte =null;
		try {
			fileByte = Files.readAllBytes(Paths.get(absolutePath));
		} catch (Exception e) {
			System.out.println("Error\n"+e);
		}
		return fileByte;	
	}
	/**
	 * Funcio de escriptura dels fitxers
	 * @param archive  Nom amb extencio del fitxer 
	 * @param filebyte bytes amb el contingut del fitxer 
	 * @param coder Indica si esta codificat amb true o descodificat en false
	 */
	public static void fileWrite(String archive, byte[] filebyte, boolean coder) {
		String letter = (coder)?"_X":"_Y";
		String nameFile = archive.substring(0,archive.indexOf('.'));
		String extencion = archive.substring(archive.indexOf('.'));
		String fileNew = nameFile+letter+extencion;
		System.out.println("\nCreant Fitxer....");
		try {

			File file = new File(dir+fileNew);
			//check si existeix arxiu
			if(!file.exists()) {
				file.createNewFile();
			}
			//omplim el arixu amb els byte
			FileOutputStream fileStream = new FileOutputStream(file);
			fileStream.write(filebyte);
			fileStream.flush();
			fileStream.close();
			//avis usuari
			System.out.println("\nFitxer: "+fileNew+" creat correctament");
		} catch (Exception e) {
			System.out.println("\nError al crear el fitxer: "+fileNew+"\nError:"+e);
		}

	}



	public static void main(String[] args) {

		String archive = ("test.txt");
		//fileWrite(archive, fileReader(archive), false);

		ManageFile test = new ManageFile("files");
		System.out.println(test.getDir());
		System.out.println(test.urlDir(archive));
		
	}

}
