import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Scanner;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

public class A5 {

	/**
	 * Genera un parell de claus segons logintiud introduida.
	 * @param longuitudClau
	 * @return keys conte la clau publica y la clau privada
	 */
	public static KeyPair randomGenerate(int longuitudClau) {
		KeyPair keys = null;
		try {
			KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
			keyGen.initialize(longuitudClau);
			keys = keyGen.genKeyPair();
		}
		catch (Exception ex) {
			System.err.println("Generador no disponible.");
		}
		return keys;
	}

	/**
	 * Method per printar una Array de Byte
	 * @param data
	 */
	public static void printByte(byte[] data) {
		try {
			int count =0;
			for (byte b : data) {
				System.out.print(String.format("%8s",Integer.toBinaryString(b & 0xFF)).replace(' ','0')+" => ");
				System.out.println(data[count]);
				count++;
			}
		} catch (Exception e) {
			System.out.println("Error "+e);
		}
	}
	/**
	 * Method de desencrytacio dades
	 * @param data byte[] text en byte
	 * @param pub Privatekey clau privada
	 * @return retorna byte[] contingut 
	 */

	public static byte[] dencryptData(byte[] data, PrivateKey pub) {
		byte[] encryptedData = null;
		try {
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding","SunJCE");
			cipher.init(Cipher.DECRYPT_MODE, pub);
			encryptedData = cipher.doFinal(data);
		}
		catch (Exception ex) {
			System.err.println("Error xifrant: " + ex);
		}
		return encryptedData;
	}

	/**
	 * Method de encrytacio dades
	 * @param data byte[] text en byte
	 * @param pub Publickey clau publica
	 * @return retorna byte[] contingut 
	 */

	public static byte[] encryptData(byte[] data, PublicKey pub) {
		byte[] encryptedData = null;
		try {
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding","SunJCE");
			cipher.init(Cipher.ENCRYPT_MODE, pub);
			encryptedData = cipher.doFinal(data);
		}
		catch (Exception ex) {
			System.err.println("Error xifrant: " + ex);
		}
		return encryptedData;
	}

	public static KeyPair keys(int longuitudClau, String text) {
		
		KeyPair keys = randomGenerate(longuitudClau);
		return keys;
	}
	/**
	 * Method que printa la contrasenya encriptada i desncripte i imprimex el resultat
	 * @param keys
	 * @param text
	 */
	public static void printkeys(KeyPair keys , String text) {
		byte[] data = text.getBytes();
	    PrivateKey keyPrivate = keys.getPrivate();
		PublicKey keyPublic = keys.getPublic();
		///encrypt 
		byte[] encriptdata = encryptData(data,keyPublic);
		String encrypPrint = new String(encriptdata);
		//dencrypt
		byte[] desencriptdata = dencryptData(encriptdata, keyPrivate);
		String print = new String(desencriptdata);
		System.out.println("\nEncrypt: "+encrypPrint+"\nDencrypt: "+print);
		
		}
	/*
	 * Activitat no Dual part2
	 */
	public static void actnoDualpart2() {
		String text ="text";
		int longuitudClau = 1728;				
		boolean ctr=true;
		while(ctr) {
			long startTime = System.currentTimeMillis();
			try {
				long time=0;
				for (int i = 0; i <= 100; i++) {
					keys(longuitudClau, text);
					long endTime = System.currentTimeMillis();
					time = (endTime - startTime);
					if(i%10==0) {
						System.out.print(" "+i+"%");
					}
				}
				
				System.out.println("\nTemps " + time+ " milliseconds");
				System.out.println(longuitudClau);
				longuitudClau+=16;
				ctr=(time<=30000)?true:false;
			} catch (Exception e) {
				longuitudClau+=16;
				long endTime = System.currentTimeMillis();
				long time = (endTime - startTime);
				System.out.println("Temps " + time+ " milliseconds");
				System.out.println(longuitudClau);
				ctr=(time<=30000)?true:false;
			}
			
			
		}
	}
	/*
	 * Activitat no Dual part
	 */
	public static void actnoDualpart1() {
		String text ="text";
		int longuitudClau = 128;	
		boolean ctr=true;
		while(ctr) {
			try {
				//crea un parell de claus
				KeyPair keys = keys(longuitudClau, text);
				//incrementem en 16 la longitud de la clau
				longuitudClau+=16;
				//si keys es null manetenim el bucle, si no el trenquem
				ctr = (keys == null)?true:false;
			} catch (Exception e) {
				//incrementem en 16 la longitud de la clau
				longuitudClau+=16;
			}
		}
		//mostrem la longitud
		System.out.println(longuitudClau);
	}
	/*
	 * Activitat Dual
	 */
	public static void actDual(String text) {
		int lengthbyte = 512;
		KeyPair keys = keys(lengthbyte,text);
		printkeys(keys, text);
		
	}

	public static void main(String[] args) {
		Scanner teclat = new Scanner(System.in);
		System.out.println("Introdueix frase");
		String text = teclat.nextLine();
		actDual(text);
		System.out.println("\nPart No dual\nCalcul de mida minima a generar");
		actnoDualpart1();
		System.out.println("\nPart No dual\nCalcul de mida de bytes fins a 30 seg");
		actnoDualpart2();

		teclat.close();

	}


}
