import java.util.Scanner;

public class code {
	//declarar variables
	private static String abc ="a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z, ";
	private static String[] array = abc.split(",");
	
	
	/**
	 * Funcio de codificacio
	 * @param int num numero de codificacio
	 * @param String secret paraula a codificar
	 * @return String coding String amb la paraula codificada
	 */
	public static String coding(int num, String secret) {
		// array que guarda la contraseņa
		String[] secretArray = secret.split("");
		// variable guarda el string codificat
		String coding = "";
		//per cada lletar del array de la paraula secreta 
		for (String letterSecret : secretArray) {
			for (int i = 0; i < array.length; i++) {
				// si la lletra de la paraula secreta igual a la del array
				if(letterSecret.equalsIgnoreCase(array[i])) {
					//mira si la i+num es major al array.length
					if(i+num < array.length) {
						// guarda en coding el valor de array[i+num]
					coding+=(array[i+num]);
					}else {
					// en cas contrari  la suma del (i+num)-array.length
						int count = ((i+num)-array.length);
					coding+=(array[count]);
					}
				}
			}
		}
		return coding;
	}
	
	public static void main(String[] args) {
		Scanner teclat = new Scanner(System.in);
		int num=array.length;
		while(num>array.length-1) {
		System.out.println("Introdueix numero de pass(maxim "+(array.length-1)+")");
		num = teclat.nextInt();
		}
		System.out.println("Introdueix text codificat");
		teclat.nextLine();
		String secret = teclat.nextLine().toLowerCase();
		String coding = coding(num, secret);
		System.out.println("\n"+coding);
		teclat.close();
	}

}

