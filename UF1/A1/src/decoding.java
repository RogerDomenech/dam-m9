import java.util.Scanner;

public class decoding {
	
	//declarar variables
		private static String abc ="a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z, ";
		private static String[] array = abc.split(",");
		
		
	//3 bcrcqcd	
	/*
	 * Funcio decoding
	 * @param int num numero de pass 
	 * @param String coding string codificat
	 * @return String decoding String descodificat*/
	public static String decodificar(int num, String coding) {
		//String que contindra el 
		String decoding = "";
		//array coding 
		String[] arrayCoding = coding.split("");
		//per cada letterCodign 
		for (String letterCoding : arrayCoding) {
			for (int i = 0; i < array.length; i++) {
				// si la letterCoding es igual a array 
				if(letterCoding.equalsIgnoreCase(array[i])) {
					// mira si i-num es major a 0
					if(i-num >=0) {
						decoding+=(array[i-num]);
					}else {
						// sino array.length + (i -num) per que i-num dona numero negatiu
						int count = (array.length+(i-num));
						decoding+=(array[count]);
					}
				}
			}
		}
		return decoding;
	}
	
	public static void main(String[] args) {
		Scanner teclat = new Scanner(System.in);
		int num=array.length;
		while(num>array.length-1) {
		System.out.println("Introdueix numero de pass(maxim "+(array.length-1)+")");
		num = teclat.nextInt();
		}
		System.out.println("Introdueix text codificat");
		teclat.nextLine();
		String code = teclat.nextLine().toLowerCase();
		String decoding = decodificar(num, code);
		System.out.println("\n"+decoding);
		teclat.close();

	}

}
