
import java.util.Scanner;

public class forcebrute {

	// declarar variables
	private static String abc = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z, ";
	private static String[] array = abc.split(",");

	/*
	 * Funcio decoding
	 * 
	 * @param int num numero de pass
	 * 
	 * @param String coding string codificat
	 * 
	 * @return String decoding String descodificat
	 */
	public static String decodificar(int num, String coding) {
		// String que contindra el
		String decoding = "";
		// array coding
		String[] arrayCoding = coding.split("");
		// per cada letterCodign
		for (String letterCoding : arrayCoding) {
			for (int i = 0; i < array.length; i++) {
				// si la letterCoding es igual a array
				if (letterCoding.equalsIgnoreCase(array[i])) {
					// mira si i-num es major a 0
					if (i - num >= 0) {
						decoding += (array[i - num]);
					} else {
						// sino array.length + (i -num) per que i-num dona numero negatiu
						int count = (array.length + (i - num));
						decoding += (array[count]);
					}
				}
			}
		}
		return decoding;
	}

	/*
	 * Funcio decoding per for�a
	 * 
	 * @param String coding string codificat
	 * 
	 * @return Array codes generats
	 */

	public static String[] force(String coding) {
		// array coding
		String[] arrayCoding = coding.split("");
		// logitud de la array
		int num = arrayCoding.length;
		// Array que contindra els codis generats
		String[] force = new String[num];
		// per cada element de arrayCoding
		for (int j = 0; j < arrayCoding.length; j++) {
			// fem un decodificar amb volta+1 i paraula codificada
			force[j] = decodificar(j + 1, coding);
		}
		return force;
	}

	public static void main(String[] args) {
		Scanner teclat = new Scanner(System.in);
		System.out.println("Introdueix text codificat");
		String code = teclat.nextLine().toLowerCase();
		System.out.println();
		String[] result = force(code);
		for (String decoding : result) {
			System.out.println(decoding);
		}

		teclat.close();

	}

}
