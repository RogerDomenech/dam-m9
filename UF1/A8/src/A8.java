import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Scanner;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;


public class A8 {

	// nom fitxer
	private static String filename = "test";
	// contrasenya per el magatzem test
	private static String pass= "testing";
	private static char[] password = pass.toCharArray();
	//format del magatzem
	private static String instance = "JCEKS"; //KeyStore.getDefaultType()

	//Boolan que controla tots els errors
	private static Boolean masterCtrl = true;

	/**
	 * Accces Al Magatzem segons filname
	 * @param filename variable statica indica nom del fitxer
	 * @param password variable statica indica el password
	 * @param instance varible statica indica el format del magatzem
	 * @return magatzem de claus
	 */
	public static KeyStore acceStore(String filename,char[] password,String instance) {

		KeyStore keystore = null;
		// definim el magatzem amb el format que volem
		try {
			keystore = KeyStore.getInstance(instance);
		} catch (KeyStoreException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		// indiquem el nom del fitxer que conte el arxiu
		File keystrorefile = new File("test");
		// Recorrem el arxiu 
		InputStream keystoreStream = null;
		try {
			keystoreStream = new FileInputStream(keystrorefile);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			masterCtrl=false;
		}
		// cargem el arxiu amb la contrasenya
		try {
			keystore.load(keystoreStream, password);
		} catch (NoSuchAlgorithmException e) {
			System.out.println("Error Algoritme:\n");
			e.printStackTrace();
			masterCtrl=false;
		} catch (CertificateException e) {
			System.out.println("Error Certificat:\n");
			e.printStackTrace();
			masterCtrl=false;
		} catch (IOException e) {
			System.out.println("Error Fitxer:\n"+ e.hashCode());
			e.printStackTrace();
			masterCtrl=false;
		}
		return keystore;
	}

	/**
	 * LLista per consola el contigunt del magatzem de claus
	 */

	public static void list() {

		try {
			KeyStore keystore = acceStore(filename, password, instance);
			if(masterCtrl) {
				// Guardem els Alies
				Enumeration<String> aliases= keystore.aliases();

				// amb els alies buscarem els demes elements
				System.out.println("Alies => Algoritme => Tamany => Format => Data d'expiració");

				while (aliases.hasMoreElements()) {

					//alias del algoritme
					String alias = (String) aliases.nextElement();
					// busquem la clau segons alias i password
					// en aquest cas les contrasenyes de cada clau son les mateixes.
					Key key =keystore.getKey(alias, password);
					//algoritme
					String algoritme = key.getAlgorithm();
					//size
					byte [] encode = key.getEncoded();
					String size = String.valueOf(encode.length);
					String format = key.getFormat();
					//Certificat Expiry
					//https://gist.github.com/sureshpai/8c762603969e78dc2c68
					// generm certificat amb el alies
					Certificate cert = keystore.getCertificate(alias);
					// si hi ha certificat guardem el nom, sino -
					String certType = (cert != null)?cert.getType():"-";
					// definim dataExpire per defecte
					String dataExpire ="-";
					// si es certificat public mirem el contigut
					if (certType.equalsIgnoreCase("X.509")) {
						// guardem un X509Certificate apartir del cert
						X509Certificate x509 = (X509Certificate)cert;
						// consultem la data de expiracio
						Date expire =x509.getNotAfter();
						// la formatem 
						Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						// assignem valor al String
						dataExpire = formatter.format(expire);
					}

					// impirmim resultats
					System.out.println(alias+" => "+algoritme+" => "+size+" => "+format+" => "+dataExpire);
				}
			}
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			System.out.println("Error en keyStore:\n");
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Error general:\n");
			e.printStackTrace();
		}

	}

	public static void exportCert() {
		// boolean que controla la sortida
		boolean exit = false;

		if(masterCtrl) {
			Scanner teclat = new Scanner(System.in);
			System.out.println("Introdueix alies de la clau o escriu EXIT per finalitzar");
			String alies = teclat.nextLine();
			exit =(alies.equalsIgnoreCase("exit"));
			// inici del magatzem
			KeyStore test = acceStore(filename, password, instance);
			//check alies
			try {
				if(!exit) {
					boolean ctrl = test.containsAlias(alies);
					while (!ctrl) {
						System.out.println("Alies incorrecte\nIntrodueix alies de la clau o escriu EXIT per finalitzar");
						alies = teclat.nextLine();	

						ctrl = test.containsAlias(alies);
					}
					exit =(alies.equalsIgnoreCase("exit"));
					if(!exit) {
						Certificate cert =test.getCertificate(alies);
						try {
							if(cert!=null) {
							byte[]fileData = cert.getEncoded();
							fileWrite(fileData, alies);
							}else {
								System.out.println("Has seleccionat una clau simetrica, no es pot generar certificat");
							}
						} catch (CertificateEncodingException e) {
							e.printStackTrace();
						}
					}
				}
			} catch (KeyStoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				teclat.close();
			}
		}

		if(exit) {
			System.out.println("End Program");
		}		
	}

	/**
	 * Funcio de escriptura dels fitxers
	 * @param filebyte bytes amb el contingut del fitxer 
	 * @param nameFile String amb el nom del fitxer
	 * @param coder Indica si es clau amb true o missatge en false
	 */
	public static void fileWrite(byte[] filebyte,String nameFile) {

		String fileNew = nameFile+".crt";
		System.out.println("\nCreant Fitxer....");
		try {

			File file = new File(fileNew);
			//check si existeix arxiu
			if(!file.exists()) {
				file.createNewFile();
			}
			//omplim el arixu amb els byte
			FileOutputStream fileStream = new FileOutputStream(file);
			fileStream.write(filebyte);
			fileStream.flush();
			fileStream.close();
			//avis usuari
			System.out.println("\nFitxer: "+fileNew+" creat correctament");
		} catch (Exception e) {
			System.out.println("\nError al crear el fitxer: "+fileNew+"\nError:"+e);
		}

	}
	public static void main(String[] args) {
		list();
		System.out.println("\n");
		exportCert();		
	}
}
