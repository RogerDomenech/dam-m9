import java.io.File;
import java.io.FileOutputStream;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;

public class Generacio {

	private static int keySize = 512;

	/**
	 * Genera un parell de claus segons logintiud introduida.
	 * @param longuitudClau
	 * @return keys conte la clau publica y la clau privada
	 */
	public static KeyPair randomGenerate() {
		KeyPair keys = null;
		try {
			KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
			keyGen.initialize(keySize);
			keys = keyGen.genKeyPair();
		}
		catch (Exception ex) {
			System.err.println("Generador no disponible.");
		}
		return keys;
	}

	/**
	 * Funcio de escriptura dels fitxers
	 * @param archive  Nom amb extencio del fitxer 
	 * @param filebyte bytes amb el contingut del fitxer 
	 * @param coder Indica si es clauPublica amb true o clauPRIVADA en false
	 */
	public static void fileWrite(byte[] filebyte, boolean coder) {
		String nameFile = (coder)?"clauPublica":"clauPRIVADA";
		System.out.println("\nCreant Fitxer....");
		try {

			File file = new File(nameFile);
			//check si existeix arxiu
			if(!file.exists()) {
				file.createNewFile();
			}
			//omplim el arixu amb els byte
			FileOutputStream fileStream = new FileOutputStream(file);
			fileStream.write(filebyte);
			fileStream.flush();
			fileStream.close();
			//avis usuari
			System.out.println("\nFitxer: "+nameFile+" creat correctament");
		} catch (Exception e) {
			System.out.println("\nError al crear el fitxer: "+nameFile+"\nError:"+e);
		}

	}

	public Generacio() {
		// --- Generació.java ----
		// generem la clau aleatoria
		KeyPair keys = randomGenerate() ;
		//creem un arxiu clauPublica per keypublic
		PublicKey keyPublic = keys.getPublic();
		byte[] dataPublic = keyPublic.getEncoded();
		fileWrite(dataPublic, true);
		// creem un arxiu clauPRIVADA per keyprivate
		PrivateKey keyPrivate = keys.getPrivate();
		byte[] dataPrivate =keyPrivate.getEncoded();
		fileWrite(dataPrivate, false);
	}

	public static void main(String[] args) {
		// --- Generació.java ----
		Generacio test = new Generacio();
	}

}
