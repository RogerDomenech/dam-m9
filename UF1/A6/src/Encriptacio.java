import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Scanner;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class Encriptacio {
	/**
	 * Funcio de escriptura dels fitxers
	 * @param filebyte bytes amb el contingut del fitxer 
	 * @param coder Indica si es clau amb true o missatge en false
	 */
	public static void fileWrite(byte[] filebyte, boolean coder) {
		String letter = "ZZZ_";
		String nameFile = (coder)?"clau_encriptada":"missatge_encriptat";
		String fileNew = letter+nameFile;
		System.out.println("\nCreant Fitxer....");
		try {

			File file = new File(fileNew);
			//check si existeix arxiu
			if(!file.exists()) {
				file.createNewFile();
			}
			//omplim el arixu amb els byte
			FileOutputStream fileStream = new FileOutputStream(file);
			fileStream.write(filebyte);
			fileStream.flush();
			fileStream.close();
			//avis usuari
			System.out.println("\nFitxer: "+fileNew+" creat correctament");
		} catch (Exception e) {
			System.out.println("\nError al crear el fitxer: "+fileNew+"\nError:"+e);
		}

	}

	/**
	 * Funcio de lectura dels fitxers
	 * @param archive Nom amb extencio del fitxer 
	 * @return filebytes bytes amb el contingut del fitxer 
	 * */ 
	public static byte[] fileReader(String archive){
		String absolutePath = new File(archive).getAbsolutePath();
		byte[] fileByte =null;
		try {
			fileByte = Files.readAllBytes(Paths.get(absolutePath));
		} catch (Exception e) {
			System.out.println("Error\n"+e);
		}
		return fileByte;	
	}
	/**
	 * Method de encrytacio dades
	 * @param data byte[] text en byte
	 * @param pub Publickey clau publica
	 * @return retorna byte[] contingut 
	 */

	public static byte[] encryptData(byte[] data, PublicKey pub) {
		byte[] encryptedData = null;
		try {
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding","SunJCE");
			cipher.init(Cipher.ENCRYPT_MODE, pub);
			encryptedData = cipher.doFinal(data);
		}
		catch (Exception ex) {
			System.err.println("Error xifrant: " + ex);
		}
		return encryptedData;
	}

	/**
	 * Funcio que genera un SecretKey
	 * @param  text text a codificar 
	 * @return SecretKey sKey objecte amb la key creada
	 */

	public static SecretKey passwordKeyGeneration(String text ) {
		SecretKey sKey= null;
		String codeHash = "MD5";
		int keySize = 128;

		try {
			byte[] data= text.getBytes("UTF-8");
			MessageDigest md = MessageDigest.getInstance(codeHash);
			byte[] hash = md.digest(data);
			byte[] key = Arrays.copyOf(hash, keySize/8);
			sKey = new SecretKeySpec(key,"AES");
		}catch(Exception ex) {
			System.err.println("Error generant la clau: "+ex);
		}
		return sKey;
	}
	/*
	 * Encriptacio 
	 * @param  sKey Secretkey genererat 
	 * @param  data entrada passada a binari
	 * @return text binari codificat
	 */
	public static byte[] encryptData(SecretKey sKey, byte[] data) {
		byte[] encryptedData = null;
		try {
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, sKey);
			encryptedData = cipher.doFinal(data);
		}
		catch (Exception ex) {
			System.err.println("Error xifrant les dades: " + ex);
		}
		return encryptedData;
	}
	public Encriptacio() {
		// --- Encriptació.java ---
		// demanenm el arxiu de claupublica
		Scanner teclat = new Scanner(System.in);
		System.out.println("Indiqueu el nom del fitxer amb la keyPublic");
		String archive = teclat.next();
		teclat.nextLine();
		// llegim el arixiu de la keyPublic
		byte[] fileBytePublic = fileReader(archive);
		// passem de bye a keypublic, utilitzem KeyFactory
		KeyFactory factory=null;
		try {
			factory = KeyFactory.getInstance("RSA");
		} catch (Exception e) {
			System.out.println("Error KeyFactory: "+e);
		}
		// utilitzem el methode generatePublic amb el format X509EncodedKeySpec
		PublicKey keyPublic=null;
		try {
			keyPublic= factory.generatePublic(new X509EncodedKeySpec(fileBytePublic));
		} catch (Exception e) {
			System.out.println("Error KeyPublic: "+e);
		}

		// demanem text a codificar
		System.out.println("Introdueix text a encriptar");
		String text = teclat.nextLine();
		teclat.close();
		// creem el arxiu ZZZ_missatge_encriptat encriptar amb la clau simetrica Secretkey
		SecretKey secretKey= passwordKeyGeneration(text);
		byte[] textbyte = text.getBytes();
		byte[] encryptedData = encryptData(secretKey, textbyte);
		fileWrite(encryptedData, false);
		// creem arxiu ZZZ_clau_encriptada es la SecretKey amb la clauPublica
		byte[] secretByte=secretKey.getEncoded();
		byte[] encryptedKey=encryptData(secretByte,keyPublic);
		fileWrite(encryptedKey, true);
	}
	public static void main(String[] args) {
		// --- Encriptació.java ---
		Encriptacio test = new Encriptacio();
	}
}
