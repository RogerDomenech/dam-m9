import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class Desencriptacio {
	/**
	 * Funcio de lectura dels fitxers
	 * @param archive Nom amb extencio del fitxer 
	 * @return filebytes bytes amb el contingut del fitxer 
	 * */ 
	public static byte[] fileReader(String archive){
		String absolutePath = new File(archive).getAbsolutePath();
		byte[] fileByte =null;
		try {
			fileByte = Files.readAllBytes(Paths.get(absolutePath));
		} catch (Exception e) {
			System.out.println("Error\n"+e);
		}
		return fileByte;	
	}
	/**
	 * Method de desencrytacio dades
	 * @param data byte[] text en byte
	 * @param pub Privatekey clau privada
	 * @return retorna byte[] contingut 
	 */

	public static byte[] dencryptData(byte[] data, PrivateKey pub) {
		byte[] encryptedData = null;
		try {
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding","SunJCE");
			cipher.init(Cipher.DECRYPT_MODE, pub);
			encryptedData = cipher.doFinal(data);
		}
		catch (Exception ex) {
			System.err.println("Error xifrant: " + ex);
		}
		return encryptedData;
	}
	/*
	 * DesEncriptacio 
	 * @param SecretKey sKey Secretkey genererat 
	 * @param byte data byte encriptat
	 * @return byte text binari codificat
	 */
	public static byte[] desEncryptData(SecretKey sKey, byte[] data) {
		byte[] encryptedData = null;
		try {
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, sKey);
			encryptedData = cipher.doFinal(data);
		}
		catch (Exception ex) {
			System.err.println("Error xifrant les dades: " + ex);
		}
		return encryptedData;
	}

	public static void main(String[] args) {
		// --- Descriptació.java ---
		// desncripte la clau_encriptada amb la clauPrivada
		// llegim el fitxer ZZZ_clau_encriptada
		byte[] clauEncriptadaByte= fileReader("ZZZ_clau_encriptada");
		// desencriptem amb la clau privada
		byte[] clauPrivadaByte = fileReader("clauPRIVADA");
		// passem de bye a private, utilitzem KeyFactory
		KeyFactory factory=null;
		try {
			factory = KeyFactory.getInstance("RSA");
		} catch (Exception e) {
			System.out.println("Error KeyFactory: "+e);
		}
		// utilitzem el methode generatePublic amb el format PKCS8EncodedKeySpec
		PrivateKey keyPrivate=null;
		try {
			keyPrivate = factory.generatePrivate(new PKCS8EncodedKeySpec(clauPrivadaByte));

		} catch (Exception e) {
			System.out.println("Error KeyPublic: "+e);
		}
		// desenciptem la clauencriptadaByte
		byte[] clauDesencriptadaByte = dencryptData(clauEncriptadaByte, keyPrivate);
		// la passem la clauencriptdaByte a secretkey 
		SecretKey originalKey = new SecretKeySpec(clauDesencriptadaByte, 0, clauDesencriptadaByte.length, "AES");
		// desencripte el missatge_encriptat amb la clau_encriptada descriptada
		// llegim el fitxer ZZZ_misstage_encriptat
		byte[] missatgeEncriptatByte = fileReader("ZZZ_missatge_encriptat");
		// desencriptem amb la clau descriptada
		byte[] missatgeDesencriptatByte = desEncryptData(originalKey, missatgeEncriptatByte);
		String missatge = new String (missatgeDesencriptatByte);
		// print pantalla el contigut ZZZ_missatge_encriptat
		System.out.println(missatge);

	}

}
