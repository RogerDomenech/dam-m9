import java.util.*;
import java.util.concurrent.*;


public class Suma {
	// la interfice Callable permet utilitzar el metode call()
	static class Sumatori implements Callable<Integer> {
		// contenidors dels operados
		private int operador1;
		private int operador2;
		
		//constructor 
		public Sumatori(int operador1, int operador2) {
			this.operador1 = operador1;
			this.operador2 = operador2;
		}
		
		@Override
		// aquest metode ens permet executar la classe quan es declara
		public Integer call() throws Exception {
			System.out.println(Thread.currentThread().getName()+"=>"+Thread.currentThread().getId());
			return operador1 + operador2;
		}
	}
	
	public static void main(String[] args) throws
	InterruptedException, ExecutionException {
		// Declara el nombre de fils a usar amb newFixedThrearPool
		ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(5);
		//llistat de taques en forma de ArrayList
		List<Sumatori> llistaTasques= new ArrayList<Sumatori>();
		// realitzem 25 passades 
		for (int i = 0; i < 26; i++) {
			// realitzem una suma utlitzant la classe Sumetori amb 2 nombres random del 0-10
			Sumatori calcula = new Sumatori((int)(Math.random()*10), (int)(Math.random()*10));
			// afegim en la llista el Sumatori 
			llistaTasques.add(calcula);
		}
		
		/*Montatge de la cua de fils*/
		// Declaracio del llistat que s'utilitzara en el "FUTUR"  per guradar els resultats
		List <Future<Integer>> llistaResultats;
		// llistaResultals contindra tots els fils amb el metode invokeAll
		llistaResultats = executor.invokeAll(llistaTasques);
		// finalitza els fil en executacio 
		executor.shutdown();
		
		// lectura del temps actual
		long startime = System.currentTimeMillis();
		//recrrem llistaResultats
		for (int i = 0; i < llistaResultats.size(); i++) {
			// restultat es la possicio actual de llistatResultats
			Future<Integer> resultat = llistaResultats.get(i);
			try {
				// immprimim text amb resultat
				System.out.println("Resultat tasca "+i+ " �s:" + resultat.get());
				// test
				//if (Thread.currentThread().getId()!=1) {
					//System.out.println(Thread.currentThread().getName()+"=>"+Thread.currentThread().getId());
				//}
				
			} catch (InterruptedException | ExecutionException e){
				System.out.println("Error: "+e);
			}
		}
		// lectura del temps actual
		long endtime = System.currentTimeMillis();
		// morstrem el temps passat d'execuci�
		System.out.println("Total Time "+(endtime-startime)+"\nProcces "+Runtime.getRuntime().availableProcessors());
		//Nombre de fils maxim a usar
		//Runtime.getRuntime().availableProcessors())
	}
}
