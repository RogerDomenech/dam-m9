import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

public class modFibonacci extends RecursiveTask<Long> {
	long numero;
	public modFibonacci(long numero){
		this.numero=numero;
	}    
	@Override
	protected Long compute() {
		// ATENCIO **1** 
		
		double calcul = java.lang.Math.cos(54879854);
		if(numero <= 1) return numero;
		modFibonacci fib1 = new modFibonacci(numero-1);
		//fib1.fork();
		modFibonacci fib2 = new modFibonacci(numero-2);
		fib2.fork();
		return fib1.compute()+ fib2.join();
	}


	public static void main(String[] args){
		ForkJoinPool pool = new ForkJoinPool();
		System.out.println("Calculat:  " + pool.invoke(new AfibonacciforkJoin(35)));    
	}
}
