import java.util.concurrent.ForkJoinPool;

public class A3_Test {
	
	// test del calcul Recursiu
	public static void testRecursive(int[]ArrayTest) {
		// foreach que recorra la array de Test
		for (int number : ArrayTest) {
			//Inici del crono
			long startTime = System.currentTimeMillis();
			//Crida al calcul del Fibonacci utilitzan la class fibonacciRecursive
			long recurseive = fibonacciRecursive.calculaFibonacci(number);
			//Fi del crono
			long endTime = System.currentTimeMillis();
			//Imprimir els resultats
			System.out.println("Numero de Fibonacci "+recurseive+" corresponent a la posicio "+number);
			System.out.println("Temps execuci�: "+(endTime-startTime)/1000+"s");
		}	
	}
	
	//test del calcul amb fils utilitzant Fork Join
	public static void testForkJoin(int[]ArrayTest) {
		ForkJoinPool pool = new ForkJoinPool();
		for (int number : ArrayTest) {
			long startTime = System.currentTimeMillis();
			long forkJoin = pool.invoke(new AfibonacciforkJoin(number)) ;  
			long endTime = System.currentTimeMillis();
			System.out.println("Numero de Fibonacci "+forkJoin+" corresponent a la posicio "+number);
			String time = (((endTime-startTime)/1000)>1)? (endTime-startTime)/1000+"s":(endTime-startTime)+"ms";
			System.out.println("Temps execuci�: "+time);
			
		}	
		pool.shutdown();
		
	}
	//test del calcul amb fils utilitzant Fork Join amb un calcul de cos per relantitzar
	public static void testForkJoinMod(int[]ArrayTest) {
		ForkJoinPool pool2 = new ForkJoinPool();
		for (int number : ArrayTest) {
			long startTime = System.currentTimeMillis();
			long forkJoin = pool2.invoke(new modFibonacci(number)) ;  
			long endTime = System.currentTimeMillis();
			System.out.println("Numero de Fibonacci "+forkJoin+" corresponent a la posicio "+number);
			String time = (((endTime-startTime)/1000)>1)? (endTime-startTime)/1000+"s":(endTime-startTime)+"ms";
			System.out.println("Temps execuci�: "+time);		
		}	
		pool2.shutdown();
	}

	//Test general dels les posicions 35 al 40 
	public static void main(String[] args) {
		// test de 35 -> 40
		System.out.println("Inici del Test");
		int[] number = {35,36,37,38,39,40};
		testRecursive(number);
		System.out.println("\n//////////////////////////\n");
		testForkJoin(number);
		System.out.println("\n//////////////////////////\n");
		testForkJoinMod(number);
		
	}

}
