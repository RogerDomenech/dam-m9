
import java.util.concurrent.RecursiveTask;
import java.util.concurrent.ForkJoinPool;
import java.lang.Thread;

public class MaximTask extends RecursiveTask<Short> {

	//definicio de variables
	private static final int LLINDAR=10000000;
	private short[] arr ;
	private int inici, fi;
	// definico de les vegades que escrida
	private static int count=1;
	//constructor
	public MaximTask(short[] arr, int inici, int fi) {
		this.arr = arr;
		this.inici = inici;
		this.fi = fi;
	}
	
	//Calcul sequencial
	//retorna el valor maxim de short[]
	private short getMaxSeq(){
		short max = arr[inici];
		for (int i = inici+1; i < fi; i++) {
			
			if (arr[i] > max) {
				max = arr[i];
			}
		}
		return max;
	}
	
	// Calcul amb fils
	// retorna el valor maxim resultant de la execucio de 2 tasques.
	private short getMaxReq(){
		MaximTask task1;
		MaximTask task2;
		// mig de la sequencia actual
		int mig = (inici+fi)/2+1;
		// Assigancio de la 1ra part del calcul
		task1 = new MaximTask(arr, inici, mig);
		task1.fork();
		// Assigancio de la 2on part del calcul 
		task2 = new MaximTask(arr, mig, fi);
		task2.fork();
		return (short) Math.max(task1.join(), task2.join());
	}
	
	
	@Override
	protected Short compute() {
		// text de sortida
		String missatge= "Comptador	"+this.count+
						"   Fil     "+(Thread.currentThread().getId()-10)+
						"	Inici	"+this.inici+
						"	Fi	    "+this.fi;
					
		System.out.println(missatge);
		this.count++;
		// comprovacio del llindar
		// si el llindar es major que que diferencia entre el inici i final 
		// Executa el calcul sequencial
		if((fi-inici) <= LLINDAR){
			return getMaxSeq();
		// En cas contrarri executara el calcul amb els fils
		}else{
			return getMaxReq();
		}
		
	}

	public static void main(String[] args) {

		short[] data = createArray(100000000);

		// Mira el n�mero de processadors
		System.out.println("Inici c�lcul");
		ForkJoinPool pool = new ForkJoinPool();

		int inici=0;
		int fi= data.length;
		MaximTask tasca = new MaximTask(data, inici, fi);

		long time = System.currentTimeMillis();
		// crida la tasca i espera que es completin
		int result1 = pool.invoke(tasca);
		// m�xim
		int result= tasca.join();
		System.out.println("Temps utilitzat:" +((System.currentTimeMillis())-time));
		System.out.println ("M�xim es " + result);
	}

	// gurarda els valors en short []
	private static short [] createArray(int size){
		short[] ret = new short[size];
		for(int i=0; i<size; i++){
			ret[i] = (short) (1000 * Math.random());
			if(i==((short)(size*0.9))){
				ret[i]=1005;
			}
		}
		return ret;
	}
}		
