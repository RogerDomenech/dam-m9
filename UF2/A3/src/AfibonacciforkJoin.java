import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

public class AfibonacciforkJoin extends RecursiveTask<Long> {
	long numero;
	public AfibonacciforkJoin(long numero){
		this.numero=numero;
	}    
	@Override
	protected Long compute() {
		// ATENCIO **1** 
		//
		//double calcul = java.lang.Math.cos(54879854);
		if(numero <= 1) return numero;
		AfibonacciforkJoin fib1 = new AfibonacciforkJoin(numero-1);
		//fib1.fork();
		AfibonacciforkJoin fib2 = new AfibonacciforkJoin(numero-2);
		fib2.fork();
		return fib1.compute()+ fib2.join();
	}


	public static void main(String[] args){
		ForkJoinPool pool = new ForkJoinPool();
		System.out.println("Calculat:  " + pool.invoke(new AfibonacciforkJoin(35)));    
	}
}
