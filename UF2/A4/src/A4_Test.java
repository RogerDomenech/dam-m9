import java.util.concurrent.ForkJoinPool;
public class A4_Test {
	public static void main(String[] args) {
		int test = 3;
		for (int i = 0; i < test; i++) {
			//int max=i+1;
			int max = 15;
			System.out.println("Numero de Test: "+(i+1)+"\nValor maxim a buscar: "+max);
			//recursiu
			System.out.println("Recursiu de 0 a "+max);
			long startTime = System.currentTimeMillis();
			for (int numero = 0; numero<=max; numero++) {

				long startTimePart = System.currentTimeMillis();
				NumeroCatalan nombre = new NumeroCatalan(numero);
				//long endTimePart = System.currentTimeMillis();
				//String time = ((endTimePart-startTimePart)/1000<0)?(endTimePart-startTimePart)/1000+"s":(endTimePart-startTimePart)+"ms";
				//long time =System.currentTimeMillis()-startTimePart;
				System.out.println("Numero a calcular: "+numero+" Numero resultant: "+nombre.recursiu(numero)+" => Temps:"+(System.currentTimeMillis()-startTimePart));
				
			}
			long endTime = System.currentTimeMillis();
			//String time = ((endTime-startTime)/1000<0)?(endTime-startTime)/1000+"s":(endTime-startTime)+"ms";
			String time =(endTime-startTime)+"ms";
			System.out.println(time);
			System.out.println("ForkJoin de 0 a "+max);
			long startTime2 = System.currentTimeMillis();
			for (int numero = 0; numero<=max; numero++) {
				//ForkJoin
				//System.out.println("ForkJoin");
				long startTimePart2 = System.currentTimeMillis();
				ForkJoinPool pool = new ForkJoinPool();
				long endTimePart2 = System.currentTimeMillis();
				//String time2 = ((endTimePart-startTimePart)/1000<0)?(endTimePart-startTimePart)/1000+"s":(endTimePart-startTimePart)+"ms";
				String time2 =(endTimePart2-startTimePart2)+"ms";
				System.out.println("Numero a calcular: "+numero+" Numero resultant: "+pool.invoke(new NumeroCatalan(numero))+" => Temps:"+(System.currentTimeMillis()-startTimePart2));
				//pool.invoke(new NumeroCatalan(numero));
			}
			long endTime2 = System.currentTimeMillis();
			String time2 =(endTime2-startTime2)+"ms";
			System.out.println(time2);
			
		}
	}
}
