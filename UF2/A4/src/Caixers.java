import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

public class Caixers  {

	static class Client implements Callable<ArrayList<Integer>>{
		//propietats client
		// carro es contingut de articles
		private ArrayList<Integer> carro = new ArrayList<Integer>();
		// numero de Client
		private int n;
		
		public Client(int n) {
			this.n=n;
			this.carro = compra();
		}
		// compra es el methode de generar quantitat
		//	d'articles dintre de carro

		public static ArrayList<Integer> compra() {
			ArrayList<Integer> carroClient = new ArrayList<Integer>();
			// Cada client porta un nombre aleatori (d�entre 1 i 30) articles 
			int quantitatArticle = (int) (Math.random()*30+1);
			for (int i = 0; i < quantitatArticle; i++) {	
				carroClient.add(article());
			}
			return carroClient;
		}
		public static int article() {
			int article = (int) (Math.random()*10+1);
			return article;
		}
		public ArrayList<Integer> getCarro() {
			return carro;
		}
		@Override
		public ArrayList<Integer> call() throws Exception {
			// retard de 3 segons
			Long segRetard = (long) 3000;
			Thread.sleep(segRetard);
			// avis de client per caixa
			System.out.println("Client "+(n+1)+" passa per caixa...");
			// llegim el contingu del carro
			ArrayList<Integer> contigut = carro;
			int countArticle = 1;
			for (Integer article : carro) {
				// lectura del temps actual
				long startime = System.currentTimeMillis();
				//cada article en q�esti� triga un temps a passar per caixa (d�entre 2 i 8 segons) 
				int segRetad = (int)(Math.random()*8+2);
				Thread.sleep((long)segRetad*1000);
				// impirmir numero de client
				// imprimir numero de article actual
				// impromir numero total d'articles
				int sizeContigut = carro.size();
				// temps en porcesar article
				long endtime = System.currentTimeMillis();
				int time = (int)(endtime-startime)/1000;
				String missatge = "Client "+(n+1)+" article "+countArticle+"/"+sizeContigut+" ("+time+" segons)...";
				missatge=(countArticle==sizeContigut)?missatge+="FINALITZAT":missatge;
				System.out.println(missatge);
				countArticle++;
				
			}
			return carro;  


		}
	}

		public static void main(String[] args) throws InterruptedException, ExecutionException {

			// crea pool
			int caixesObertes = 3;
			ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(caixesObertes);
			int maxClients= 5;
			List<Client> llistaClients = new ArrayList<Client>();
			for (int i = 0; i < maxClients; i++) {
				Client client = new Client(i);
				// afeix client a llistat
				llistaClients.add(client);
				// missatge d'avis
				ArrayList<Integer>clientCarro = llistaClients.get(i).getCarro();
				int sizeCarro = clientCarro.size();
				System.out.println("Creat el client "+(i+1)+" amb "+sizeCarro+" articles "+clientCarro);
			}
			//montar cua de fils
			List<Future<ArrayList<Integer>>> llistaCarros;
			llistaCarros= executor.invokeAll(llistaClients);
			executor.shutdown();

		}	

}

