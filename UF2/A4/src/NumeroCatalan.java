import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;


public class NumeroCatalan extends RecursiveTask<Long>{
	//propietats
	int n;
	
	public NumeroCatalan (int number) {
		this.n=number;
	}

	public int getN() {
		return n;
	}

	public Long recursiu (int n)
	{
		Long numCatalan=(long) 0;
		//double calcul = java.lang.Math.cos(54879854);
		if (n==0){
			return (long)1;
		}                      
		
		//return (((2*((2*n)-1))/(n+1))*recursiu (n-1));
		for (int i = 0; i < n; i++) { 
			numCatalan += recursiu(i) * recursiu(n - i - 1); 
        } 
        return numCatalan; 
		
	}

	@Override
	protected Long compute() {
		try {
			//System.out.println(Thread.currentThread().getName());
			double calcul = java.lang.Math.cos(54879854);
			if (n<=0) {
				return (long)1;
			}
			int numCatalan=0;
			
			//NumeroCatalan task1;
			//NumeroCatalan task2;
			for (int i = 0; i < n; i++) { 
				
			NumeroCatalan task1 = new NumeroCatalan(i);
			task1.fork();
			NumeroCatalan  task2 = new NumeroCatalan(n-i-1);
		    task2.fork();
		    numCatalan += task1.join()*task2.join();
			}
			return (long)numCatalan;
		} catch (Exception e) {
			System.out.println(Thread.currentThread().getName()+"\n"+e);
			return (long)0;
		}
				
	}
	
}
