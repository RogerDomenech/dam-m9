
import java.awt.*;
import java.util.*;
import javax.swing.*;
import java.awt.event.*;

public class NauEspaial extends javax.swing.JFrame {    
	static NauEspaial f;
	public NauEspaial() {
		initComponents();
	}

	@SuppressWarnings("unchecked")
	private void initComponents() {
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		setBackground(new java.awt.Color(255, 255, 255));
		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 400, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 300, Short.MAX_VALUE));
		pack();
	}

	public static void main(String args[]) {
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		}
		catch (Exception ex) {
			java.util.logging.Logger.getLogger(NauEspaial.class.getName()).log(
					java.util.logging.Level.SEVERE, null, ex);
		}       
		f = new NauEspaial();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setTitle("Naus Espaials");
		f.setContentPane(new PanelNau());
		f.setSize(480, 560);
		f.setVisible(true);
	}
	public static void close() { 
		//missatge de avis 
		System.out.println("\nFinalitza partida");
		//interropto els fils de PanelNau i els seus submgrups, per fer un tancament mes controlat
		PanelNau.getThreadgroup().interrupt();
		//tanco programa
		System.exit(0);
	}
}


class PanelNau extends JPanel implements Runnable, KeyListener{   
	// numero total de naus
	private int numNaus=10;
	// ArrayList de les naus creades
	static ArrayList<Nau>naus=new ArrayList<Nau>();
	// nau del juagor
	Nau nauPropia;
	// nom de la nau del jugador
		static String nauNostra = "NauNostra";
	// Objecte disparo del jugador
	static Disparo disparo;
	// objecte que s'encarrga de controlar les colisions
	Colisio colisio;
	// quantitat de desplaçanment de les naus
	int desplasament;
	
	// Grup de fils que pertany aquesta clase
	// Aquest es el grup principal 
	static ThreadGroup panelnau = new ThreadGroup("panelNau");
	
	
	public PanelNau(){        

		// Creaccio de le naus enemiges
		for (int i=0;i<numNaus;i++) {
			Random rand = new Random();
			int velocitat=(rand.nextInt(3)+5)*10;
			int posX=rand.nextInt(100)+30;
			int posY=rand.nextInt(100)+30;
			int dX=rand.nextInt(3)+1;
			int dY=rand.nextInt(3)+1;
			String nomNau = Integer.toString(i);
			//afegim en la ArrayList
			naus.add(new Nau(nomNau,posX,posY,dX,dY,velocitat));
		}

		// Creo la nau propia
		nauPropia = new Nau(nauNostra,200,400,10,0,100);
		Image imgNauPropia = new ImageIcon(Nau.class.getResource("/images/nau.png")).getImage();
		nauPropia.setImage(imgNauPropia);
		// inovilitzem la nau al inici 
		nauPropia.setdsx(0);
		nauPropia.setdsy(0);
		// assignacio del despaçament
		desplasament = nauPropia.getdesplasament();
		// cambiem el desplaçament de la nau propia
		nauPropia.setdesplasament(9);

		// Creo fil per anar pintant cada 0,1 segons el joc per pantalla
		// el fil pertany al grup panelnau
		Thread n = new Thread(panelnau,this);
		n.start();         

		// Creo listeners per a que el fil principal del programa gestioni
		// esdeveniments del teclat
		addKeyListener(this);
		setFocusable(true);               

	}

	public  void run() {
		System.out.println("Inici fil repintar");
		while(true) {
			try { Thread.sleep(100);} catch(Exception e) {} // espero 0,1 segons
			//System.out.println("Repintant");
			// si no existeix disparo
			if(disparo!=null) {
				// comprovem si hi ha colicio crean aquesta classe 
				colisio = new Colisio(disparo, naus);
			}
			// Printem les naus 
			repaint();   
		
			//Condicio de finalitzacio del programa
			if(naus.size()==0) {
				NauEspaial.close();
			}
		}                   
	}

	// utilitzem synchronized per coordinar els fils printen cada anu
	public synchronized void paintComponent(Graphics g) {

		super.paintComponent(g);
		// printem les naus enemgies
		for (int i = 0; i < naus.size(); i++) {
			Nau nau = naus.get(i);
			nau.pinta(g);
		}
		// printem la nau Propia
		nauPropia.pinta(g);
		
	    //Mirem si existeix disparo
		if(disparo!=null) {
			// printem disparo 
			disparo.pinta(g);
		}

	}


	// Metodes necesaris per gestionar esdeveniments del teclat
	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {

		//System.out.println("Key pressed code=" + e.getKeyCode() + ", char=" + e.getKeyChar());
		if (e.getKeyCode()==37) { nauPropia.esquerra();} //System.out.println("a l'esquerra"); }
		if (e.getKeyCode()==39) { nauPropia.dreta(); } //System.out.println("a la dreta"); }  
		// tecla espai dispara
		if (e.getKeyCode()==32&& disparo==null) { disparo = new Disparo(nauPropia.getx(),400,nauPropia);}
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	//method que retorna el grup de fils
	public static ThreadGroup getThreadgroup() {
		return panelnau;
	}
	
}

//Classe que controla les naus
class Nau extends Thread {
	//nom de la nau
	private String nomNau;
	// cooredeandes
	private int x,y;
	// direccio i velositat
	private int dsx,dsy,v;
	// desplaçament x
	private int tx = 10;
	// despalçament y 
	private int ty = 10;
	// desplaçament general
	private int desplasament;
	// ubicacio de la imatge de la nau
	private String img = "/images/ufo.png";
	// objecte imatge
	private Image image;
	// fil que correspont
	private Thread t;
	// gurp de fil, aquest pertany al grup naus que es subgrup de panelnau
	static ThreadGroup naus = new ThreadGroup(PanelNau.getThreadgroup(),"naus");

	public Nau(String nomNau, int x, int y, int dsx, int dsy, int v ) {
		this.nomNau = nomNau;
		this.x=x;
		this.y=y;
		this.dsx=dsx;
		this.dsy=dsy;
		this.v=v;
		this.desplasament=10;
		image = new ImageIcon(Nau.class.getResource(img)).getImage();
		this.t = new Thread(naus,this);
		t.start();
	}

	// seter & getters
	public void setImage(Image image) {
		this.image = image;
	}
	public void setdsx(int dsx) {
		this.dsx=dsx;
	}
	public void setdsy(int dsy) {
		this.dsy = dsy;
	}
	public int getdsx() {
		return this.dsx;
	}
	public int getdsy() {
		return this.dsy;
	}
	public int getx() {
		return this.x;
	}
	public int gety() {
		return this.y;
	}
	public String getNom() {
		return this.nomNau;
	}
	public Thread getThread() {
		return this.t;
	}
	public int velocitat (){
		return v;
	}

	public void setdesplasament(int desplasament) {
		if(desplasament>1&&desplasament<30){
			this.desplasament = desplasament;
		}
	}
	public int getdesplasament() {
		return this.desplasament;
	}

	// metode de moviement
	public void moure (){
		x=x + dsx;
		y=y + dsy;
		// si arriva als marges ...
		if ( x>= 450 - tx || x<= tx) dsx = - dsx;
		if ( y >= 450 - ty || y<=ty ) dsy = - dsy;

	}
	// metode de printar
	public void pinta (Graphics g) {
		Graphics2D g2d = (Graphics2D)g;
		g2d.drawImage(this.image, x, y, null);
	}


	public void run() {
		while (true) {
			//System.out.println("Movent nau numero " + this.nomNau);
			try { Thread.sleep(this.v); } catch (Exception e) {}
			moure();
		}
	}

	public void esquerra() {
		if (this.x>=0) {
			this.dsx = -desplasament;
		}else {
			dreta();
		}
		//System.out.println(this.x);
	}

	public void dreta() {
		if (this.x<=425) {
			this.dsx = desplasament;
		}else {
			esquerra();
		}
		//System.out.println(this.x);
	}
	// retorn el grup de fil que pertany
	public static ThreadGroup getThreadgroup() {
		return naus;
	}

}

// classe que controla els disparos
class Disparo extends Thread {
	// altura maxiam
	private int MaxY = 500;
	// posicio x y
	private int x,y;
	// increment de posicio
	private int dsy = 1;
	// velocitat
	private int v = 100;
	// directori de la imatg del disparo
	private String img = "/images/shoot.png";
	// imatege del disparo
	private Image image;
	// nau que dispara
	private Nau nau;
	// Grups de fils, pertany a disparos que es subgrup de nau 
	ThreadGroup disparos = new ThreadGroup(Nau.getThreadgroup(),"disparos");

	public Disparo(int x, int y, Nau nau) {
		this.x = x;
		this.y = y;
		this.image = new ImageIcon(Nau.class.getResource(img)).getImage();
		this.nau = nau;
		Thread d = new Thread(disparos,this);
		d.start();
	}
	public void pinta (Graphics g) {
		Graphics2D g2d = (Graphics2D)g;
		g2d.drawImage(this.image, x, y, null);
		//System.out.println("FIRE! "+x+" "+y);
	}
	public void moure (){
		this.y-= this.dsy;
		this.dsy++;
	}
	public void run() {
		// Si el disparo es de la nau del jugador
		if(this.nau.getName()!=PanelNau.nauNostra) {
			// de 500 a 0 per a que el dipsaro vagi de baix a dalt
			while (y>0) {
				//System.out.println("Movent nau numero " + this.nomNau);
				try { Thread.sleep(this.v); } catch (Exception e) {}
				moure();
			}
			// anulem el disparo al final per evitar el pinta en Panel
			PanelNau.disparo =null;
		}
	}
	// Getters & setters
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}

}
//Classe que controa la colisio de elements
class Colisio extends Thread{
	// disparos
	private Disparo disparo;
	// naus
	private ArrayList<Nau> naus;
	// group de fils
	// aquest perntay a colisio que es submgrup de nau 
	ThreadGroup colisio = new ThreadGroup(Nau.getThreadgroup(),"colisio");
	
	// Constructor amb disparo
	public Colisio ( Disparo disparo, ArrayList<Nau> naus) {
		this.disparo=disparo;
		this.naus= naus;
		Thread c = new Thread(colisio,this);
		c.start();

	}

	// comprovacio de coolicions
	public boolean check (int range1,int obj1 ,int range2, int obj2 ) {
		// Comprova si el obj1 i obj2 estan dintre dels marges establers segons parametres
		if(((obj1 + range1)>(obj2-range2))&&((obj1-range1)<(obj2+range2))) {
			return true;
		}else {
			return false;
		}
	}

	// usem synchronize ja que ha de realitzar diverses comprovacios mentres existixin els elements a revisar
	public synchronized void run() {

		//System.out.println("Comprovant impacte"+this.getName());
		// marge que li donem al disparo, determinat per 1/2 de longitud i amplada de la imatge de disparo
		int rangeDisparoX=7;
		int rangeDisparoY=25;
		// possicio de disparo 
		int xDisparo = this.disparo.getX();
		int yDisparo = this.disparo.getY();
		// marge que  li domen a la nau, determinat per la 1/2 de la longitud i amplada de la imatge de la nau 
		int rangeNauX=12;
		int rangeNauY=12;
		
		//recorrem la arrayList de naus 
		for (int i = 0; i < naus.size(); i++) {
			Nau nau = naus.get(i);
			// si X i Y de la nau concorden amb el disparo 
			if(check(rangeNauX,nau.getx(),rangeDisparoX, xDisparo)&&check(rangeNauY,nau.gety(),rangeDisparoY,yDisparo)) {
				System.out.println("IMPACTE!!!");
				// agafa el nom de la nau i la borra de la array list de Panel
				int index = naus.indexOf(nau);
				// interrumpim el fil de la nau corresponetn 
				nau.interrupt();
				//System.out.println("NAU"+nau.getThread().getName());
				//System.out.println("this Thread "+this.currentThread().getName());
				
				System.out.println("NAU "+nau.getNom()+" ha estat abatuda\nCoordenades X="+nau.getx()+" Y="+nau.gety());
				
				
				//borrem al nau de la array de PanelNaus
				PanelNau.naus.remove(index);

			}

		}
	}
}



